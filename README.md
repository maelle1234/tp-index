# TP-index



## Description du projet

Ce projet est réalisé dans le cadre d'un TP d'Indexation Web à l'ENSAI. L'objectif est de construire un index simple, sans positionnement, à partir d'un fichier json fourni comprenant une liste d'URLs. L'implémentation est réalisée en Python.

## Exécution du code

```
git clone https://gitlab.com/maelle1234/tp-index.git
cd tp-index
pip install -r requirements.txt
python3 main.py
```

## Fonctionnement du code

La création de l'index est réalisée via la classe `myIndex` (`index/my_index.py`), qui comporte plusieurs méthodes lui étant nécessaires. Il est nécessaire de spécifier le chemin du fichier json comprenant la liste d'URLs à l'instanciation de cette classe. On peut ensuite appeler la méthode `create_index()` qui construit l'index, puis la méthode `update_metadata()` qui produit quelques résumés statistiques de l'index créé et enfin on peut sauvegarder les résultats (index et metadata) au format json avec `save_results()`. La sauvegarde se fait dans le dossier `output`. On note aussi la présence d'autres fonctions utiles dans `index/utils.py`.

Le code permettant de créer l'index à partir de la liste d'URLs fournie, et de sauvegarder l'index et ses metadata, est contenu dans le fichier `main.py`. 

Des test unitaires ont été réalisés et sont présents dans le dossier `tests`. Pour les lancer, on peut exécuter la commande suivante:

```
python3 -m unittest
```

### Contributeurs

Maëlle Cosson