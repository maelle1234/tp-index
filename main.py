from index.my_index import myIndex

if __name__ == "__main__":
    index = myIndex('crawled_urls.json')
    index.create_index()
    index.update_metadata()
    index.save_results()