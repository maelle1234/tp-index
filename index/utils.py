import urllib.request
import socket


def get_html_content(url_page):
    """ Open and extracts content from a URL.
    
    Parameters
    ----------
    url_page : str
        URL of the page

    Return
    ------
    page_content : str
        content of the page
    """
    try:
        with urllib.request.urlopen(url_page, timeout=2) as f:
            page_content = f.read().decode('utf-8')
    except urllib.error.URLError:
        page_content = ''
    except socket.timeout:
        page_content = ''
    except ValueError:
        page_content = ''
    return page_content

def get_tokens_from_title(title):
    return title.split()

