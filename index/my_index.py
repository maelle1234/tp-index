from bs4 import BeautifulSoup
from index.utils import get_html_content, get_tokens_from_title
import json

class myIndex:
    """
    Create a simple non positional index.

    Parameters
    ----------
    urls_json_path = str
        path to the json file containing the URLs
    test : bool
        parameter used for testing during the development, 
        allows to select only the first 15 URLs

    Attributes
    ----------
    l_urls : list
        list of URLs from the give json file
    index : dict
        index created from the given URLs
    metadata : dict
        information regarding the index (stats)
    nb_total_tokens : int
        total number of tokens found (including duplicates)
    nb_pages_accessed : int
        number of pages that could be accessed (not all URLs in l_urls can be accessed)
    """

    def __init__(self, urls_json_path, test=False):

        #self.urls_json_path = urls_json_path
        with open(urls_json_path) as f:
            self.l_urls = json.load(f)

        self.index = {}
        self.metadata = {}
        self.nb_total_tokens = 0
        self.nb_pages_accessed = 0

        if test: # pour dvpt
            self.l_urls = self.l_urls[:15]


    def extract_title_from_url(self, url_page):
        """Extracts the title in a html document from its URL
        
        Parameter
        ---------
        url_page : str
            URL of the page

        Return
        ------
        str
            title of the page in lowercase
        """
        page_content = get_html_content(url_page)
        if len(page_content) != 0:
            # la page a pu être lue sans aucune erreur
            self.nb_pages_accessed += 1
            soup = BeautifulSoup(page_content, 'html.parser')
            try:
                title = str(soup.title.string)
            except AttributeError:
                title = ''            
        else: 
            title = ''
        return title.lower()


    def create_index(self):
        """ Go through each document and create the index step by step """
        print("Creating index ...")
        for i, url in enumerate(self.l_urls):
            print("Document {} out of {}".format(i+1, len(self.l_urls)))
            title = self.extract_title_from_url(url)
            new_tokens = get_tokens_from_title(title) #ex: ['karine', 'lacombe', '—', 'wikipédia']
            self.nb_total_tokens += len(new_tokens)
            #print(new_tokens)
            new_tokens_without_dup = list(set(new_tokens))
            for t in new_tokens_without_dup:
                count_t = new_tokens.count(t)
                #print(t, count_t)
                if t in self.index.keys(): # si le token est déjà présent
                    self.index[t][i] = count_t
                else: # si c'est un nouveau token
                    print("Adding new token: {}".format(t))
                    self.index[t] = {i: count_t}

    
    def get_most_frequent_token(self):
        """ Find the most frequent token in the index, ie. the token that appears the most in total
        (not in the most documents) """
        if len(self.index) != 0:
            token_occurences = {}
            for t in self.index:
                total_count = 0
                for n_doc in self.index[t].keys(): # pour chaque num de document dans le dic associé au token
                    total_count += self.index[t][n_doc]
                token_occurences[t] = total_count
            return max(token_occurences, key=token_occurences.get)
        else:
            print('Error! The index has not been created yet, or is empty. Most frequent token cannot be computed.')
            return None


    def update_metadata(self):
        """ Update metadata information from the created index """
        if len(self.index) != 0:
            self.metadata['nb_documents'] = self.nb_pages_accessed
            self.metadata['nb_tokens'] = self.nb_total_tokens
            self.metadata['mean_nb_tokens'] = round(self.nb_total_tokens/self.metadata['nb_documents'],2)
            self.metadata['most_frequent_token'] = self.get_most_frequent_token()
            #print(self.metadata)
        else:
            print('Error! The index has not been created yet, or is empty. No metadata available.')


    def save_results(self):
        """ Save both the index and the metadata in json files """
        with open('output/title.non_pos_index.json', 'w', encoding="utf-8") as f:
            json.dump(self.index, f,ensure_ascii=False, indent=4, sort_keys=True)
        with open('output/metadata.json', 'w', encoding="utf-8") as f:
            json.dump(self.metadata, f, indent=4)

