from index.utils import get_html_content
import unittest

class testUtils(unittest.TestCase):

    def test_get_html_content(self):
        url = 'https://ensai.fr/'
        content = get_html_content(url)
        self.assertIn('<!DOCTYPE html>', content)
