from index.my_index import myIndex
import unittest

class testMyIndex(unittest.TestCase):

    def test_extract_title_from_url(self):
        url = 'https://ensai.fr/'
        index = myIndex('crawled_urls.json')
        title_extracted = index.extract_title_from_url(url)
        true_title = 'Ecole d\'ingénieur statistique, data science et big data | ENSAI Rennes'.lower()
        self.assertEqual(index.nb_pages_accessed, 1)
        self.assertEqual(title_extracted, true_title)

    def test_get_most_frequent_token(self):
        index = myIndex('crawled_urls.json')
        index.index = {'exemple':{'0':1, '1':2}, 'poireau':{'1':1}}
        index.nb_total_tokens = 4
        index.nb_pages_accessed = 2
        most_freq_token = index.get_most_frequent_token()
        self.assertEqual(most_freq_token, 'exemple')

    def test_update_metadata(self):
        index = myIndex('crawled_urls.json')
        index.index = {'exemple':{'0':1, '1':2}, 'poireau':{'1':1}}
        index.nb_total_tokens = 4
        index.nb_pages_accessed = 2
        index.update_metadata()
        self.assertEqual(index.metadata['nb_documents'], 2)
        self.assertEqual(index.metadata['nb_tokens'], 4)
        self.assertEqual(index.metadata['mean_nb_tokens'], 2)

    def test_update_metadata_empty(self):
        index = myIndex('crawled_urls.json')
        index.update_metadata()
        self.assertEqual(len(index.metadata), 0)
